import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReservingComponent} from './reserving/reserving.component';
import {HomeComponent} from './home/home.component';
import { ManageComponent } from './reserving/manage/manage.component';
const routes: Routes = [
  {path:'', redirectTo:'/reserving', pathMatch: 'full'},
  {path:'reserving', component:ReservingComponent},
  {path:'home', component:HomeComponent},
  {path:'manage', component:ManageComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
