import { Component, OnInit  } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reserving',
  templateUrl: './reserving.component.html',
  styleUrls: ['./reserving.component.css']
})
export class ReservingComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  manage() {
    this.router.navigate(['manage'])
  }
  activeTab = 'search';

  search(activeTab){
    this.activeTab = activeTab;
  }

  result(activeTab){
    this.activeTab = activeTab;
  }
}
