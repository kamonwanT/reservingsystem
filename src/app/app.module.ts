import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ReservingComponent } from './reserving/reserving.component';
import {HomeComponent} from './home/home.component';
import { ManageComponent } from './reserving/manage/manage.component';




@NgModule({
  declarations: [
    AppComponent,
    ReservingComponent,
    HomeComponent,
    ManageComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
 

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
